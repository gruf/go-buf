package buf_test

import (
	"testing"

	"codeberg.org/gruf/go-buf"
)

func ensurePanic(do func()) bool {
	var r interface{}
	func() {
		defer func() {
			r = recover()
		}()
		do()
	}()
	return r != nil
}

func buffer(len, cap int) *buf.Buffer {
	return &buf.Buffer{B: make([]byte, len, cap)}
}

func TestWrite(t *testing.T) {
	b := buffer(0, 1024)
	b.Write([]byte("string"))
	if b.String() != "string" {
		t.Fatalf("Expected '%s', got '%s'", "string", b.String())
	}
	b.Reset()

	b.WriteString("string")
	if b.String() != "string" {
		t.Fatalf("Expected '%s', got '%s'", "string", b.String())
	}
}

func TestWriteByte(t *testing.T) {
	b := buffer(0, 1024)
	b.WriteByte('s')
	b.WriteByte('t')
	b.WriteByte('r')
	b.WriteByte('i')
	b.WriteByte('n')
	b.WriteByte('g')
	if b.String() != "string" {
		t.Fatalf("Expected '%s', got '%s'", "string", b.String())
	}
}

func TestWriteRune(t *testing.T) {
	b := buffer(0, 1024)
	b.WriteRune('s')
	b.WriteRune('t')
	b.WriteRune('r')
	b.WriteRune('i')
	b.WriteRune('n')
	b.WriteRune('g')
	b.WriteRune('👍')
	if b.String() != "string👍" {
		t.Fatalf("Expected '%s', got '%s'", "string", b.String())
	}
}

func TestWriteAt(t *testing.T) {
	b := buffer(0, 1024)
	b.WriteAt([]byte("string"), 0)
	b.WriteAt([]byte("string"), 1)
	b.WriteAt([]byte("string"), 4)
	if b.String() != "sstrstring" {
		t.Fatalf("Expected '%s', got '%s'", "sstrstring", b.String())
	}
	b.Reset()

	b.WriteStringAt("string", 0)
	b.WriteStringAt("string", 1)
	b.WriteStringAt("string", 4)
	if b.String() != "sstrstring" {
		t.Fatalf("Expected '%s', got '%s'", "sstrstring", b.String())
	}
	b.Reset()
}

func TestTruncate(t *testing.T) {
	b := buffer(0, 1024)
	b.WriteString("string")
	b.Truncate(2)
	if b.String() != "stri" {
		t.Fatalf("Expected '%s', got '%s'", "stri", b.String())
	}

	b.Truncate(0)
	if b.String() != "stri" {
		t.Fatalf("Expected '%s', got '%s'", "stri", b.String())
	}
}

func TestString(t *testing.T) {
	b := buffer(0, 1024)
	b.WriteString("string")
	if b.String() != "string" {
		t.Fatalf("Expected '%s', got '%s'", "string", b.String())
	}
}

func TestGuarantee(t *testing.T) {
	b := buffer(0, 0)
	b.Guarantee(10)
	if b.Len() != 0 || b.Cap() != 10 {
		t.Fatalf("Expected len:%d cap%d, got len:%d cap:%d", 0, 10, b.Len(), b.Cap())
	}
}

func TestGrow(t *testing.T) {
	b := buffer(0, 0)
	b.Grow(10)
	if b.Len() != 10 || b.Cap() != 10 {
		t.Fatalf("Expected len:%d cap%d, got len:%d cap:%d", 10, 10, b.Len(), b.Cap())
	}
}
